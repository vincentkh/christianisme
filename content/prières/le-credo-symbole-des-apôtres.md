# Le Credo - Symbole des Apôtres

{{< hint info >}}
Appelé ainsi parce qu’il est considéré comme le résumé fidèle de la foi des apôtres
{{< /hint >}}

Je crois en Dieu, le Père Tout-Puissant, créateur du ciel et de la terre;  
et en Jésus-Christ, son Fils unique, notre Seigneur, qui a été conçu du Saint-Esprit, est né de la Vierge Marie, a souffert sous Ponce Pilate, a été crucifié, est mort et a été enseveli, est descendu aux enfers.  
Le troisième jour est ressuscité des morts, est monté aux cieux, est assis à la droite de Dieu le Père Tout-Puissant, d’où il viendra juger les vivants et les morts.  
Je crois en l’Esprit-Saint, à la sainte Eglise catholique, à la communion des saints, à la rémission des péchés, à la résurrection de la chair, à la vie éternelle.

Amen.
