# Saint Francis Vocation Prayer

Most High, Glorious God, enlighten the darkness of our minds.
Give us a right faith, a firm hope and a perfect charity, so that we may always and in all things act according to Your Holy Will.
Amen.
