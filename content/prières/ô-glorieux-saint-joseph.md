# Ô glorieux saint Joseph

Ô glorieux saint Joseph, époux de Marie, accordez-nous votre protection paternelle, nous vous en supplions par le Coeur de Jésus-Christ.  
Ô vous dont la puissance infinie s’étend à toutes nos nécessités et sait nous rendre possible les choses les plus impossibles, ouvrez vos yeux de père sur les intérêts de vos enfants.  
Dans l’embarras et la peine qui nous pressent, nous recourons à vous avec confiance; daignez nous prendre sous votre charitable conduite et réglez pour nous cette affaire si importante et si difficile, cause de toutes nos inquiétudes.  
Faites que son heureuse issue tourne à la gloire de Dieu et au bien de ses dévoués serviteurs.  
Ainsi soit-il.  
Saint François de Sales
