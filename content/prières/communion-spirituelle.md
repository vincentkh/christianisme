# Communion spirituelle

Seigneur, je ne suis pas digne de te recevoir, mais dis seulement une parole et je serai guéri.

Mon Jésus, je crois que vous êtes présent dans le Saint-Sacrement.  
Je vous aime par-dessus tout chose et je désire ardemment vous recevoir.  
Puisque je ne puis, à cette heure, vous recevoir sacramentellement, venez au moins spirituellement dans mon cœur.  
Comme si vous y étiez déjà présent, je vous adore et tout entier je m’unis à vous.  
Ne permettez pas que je me sépare jamais de vous.  
Jésus, enflamme mon cœur d’amour, afin qu’il brûle toujours d’amour pour vous.  
Et vous, tous les anges et tous les saints, qui nous contemplez la face de Dieu, priez pour nous.  
Au nom du Père, du Fils et du Saint-Esprit.

Amen.
