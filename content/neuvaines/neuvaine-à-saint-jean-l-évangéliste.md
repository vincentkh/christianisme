# Neuvaine à Saint Jean l'Évangéliste

## Introduction

Saint Jean est originaire de Betsaïde, un petit village pauvre de Galilée. Son père Zébédé était pêcheur.
Lorsque Jean rencontra Jésus pour la première fois, il pêchait avec son père, son frère Jacques et Simon.
C'est à ce moment que Jésus l'appela à le suivre pour devenir un pêcheur d'hommes et sans hésiter, il abandonna tout et se mit à sa suite. (Mattieu 4:20-22)

L'amour de Jean pour Jésus est tellement grand qu'il devient son disciple bien-aimé. Il eut tout comme Simon Pierre et Jacques le privilège de monter sur la montagne Thabor afin de contempler la divinité du Verbe resplendissant dans son corps et ainsi entendre la Voix qui venue du ciel disait: "Celui-ci est mon Fils bien-aimé, en lui j'ai mis toute ma confiance, écoutez-le" (Mattieu 17:5)

Lors de l'arrestation de Jésus, Jean le suivit jusque dans la cour du grand prêtre (Jean 18:15). Et lorsqu'il fut crucifié, Jean demeura avec Marie, la mère de Jésus au pied de la croix. C'est alors que le Christ dit à sa mère: "Femme, voici ton fils." Et dit ensuite à Jean: "Voici ta mère." Et depuis cette heure-là, il la prit chez lui. (Jean 19:25)

Il vit le Christ après Sa Résurrection et reçut de lui la mission d'aller enseigner la Bonne Nouvelle avec tous les autres disciples. Il assista aussi à son ascension au Ciel, et reçut le Saint-Esprit avec les autres disciples le jour de la Pentecôte (Actes 1-2). Il fut le dernier à rester à Jérusalem, en compagnie de la Mère de Dieu, pour la servir.

## 1er jour

{{< hint info >}}
Le Verbe était la vraie lumière qui, en venant dans le monde, illumine tout homme. Il était dans le monde, et le monde fut par lui, et le monde ne l'a pas reconnu. (Jean 1:9-10)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus est vraiment le Fils de Dieu. Qu'il est venu sur la terre pour combattre durement le mal et l'injustice, afin de nous transmettre tendrement la valeur de l'amour.  
Jésus est la Lumière qui, tout comme à l'aveugle, nous rend la vue.  
Aide-moi à garder mes yeux ouverts, afin que je puisse dans mon quotidien transmettre aux personnes que je côtoie l'amour et l'affection que le Christ a mis en moi.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 2ème jour

{{< hint info >}}
"Si tu connaissais le don de Dieu et qui est celui qui te dit: "Donne-moi à boire", c'est toi qui aurais demandé et il t'aurait donné de l'eau vive."
"celui qui boira de l'eau que je lui donnerai n'aura plus jamais soif; au contraire, l'eau que je lui donnerai deviendra en lui une source jaillissant en vie éternelle" (Jean 4:10.14)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus nous propose une source qui comble déjà notre soif de bonheur, croire en lui et se mettre à son école.  
Au milieu de mes difficultés et de mes tracas, demande à Jésus de me donner l'assurance de sa présence et la certitude de son amour, afin que moi-aussi je puisse comprendre cette grande vérité qu'il a dite à la samaritaine: "si tu connaissais le don de Dieu"

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 3ème jour

{{< hint info >}}
"Que celui qui n'a jamais péché lui jette la première pierre." (Jean 8:7)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que nous somme tous pécheurs et que nous n'avons pas à juger les autres et encore moins à les condamner.  
Malheureusement, souvent dans ma vie, j'ai la mauvaise habitude de porter des jugements sévères, sans discernement et sans objectivité.  
Aide-moi à me rappeler que Jésus nous a donné comme vocation d'être ses témoins en accueillant nos frères et soeurs, tels qu'ils sont, sans jugement et sans idée préconçus.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 4ème jour

{{< hint info >}}
**Jésus se redressa et dit à la femme:**  
"Femme, où sont-ils donc? Personne ne t'a condamnée?"  
**Elle répondit:**  
"Personne, Seigneur", et Jésus lui dit: "Moi non plus, je ne te condamne pas: va et désormais ne pèche plus." (Jean 8:10-11)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus est miséricordieux, qu'il ne condamne pas, mais qu'il pardonne et m'invite à la conversion.  
Qu'il a la même compassion pour moi qu'il a eu pour la femme pécheresse.  
Que malgré mes fautes, mes manques et mes erreurs, son amour pour moi demeure inébranlable.  
Aide-moi à trouver la force et le courage, afin que je puisse vaincre mes faiblesses et mes manques d'amour, alors je pourrai me rassasier de son véritable pardon.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 5ème jour

{{< hint info >}}
"Je suis le bon berger, je connais mes brebis et mes brebis me connaissent, comme mon Père me connaît et que je connaîs mon Père; et je me dessaisis de ma vie pour les brebis. J'ai d'autres brebis qui ne sont pas de cet enclos et celles-là aussi, il faut que je les mène; elles écouteront ma voix et il y aura un seul troupeau et un seul berger." (Jean 10:14-16)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus nous aime tous également et ce peu importe notre statut, notre apparence ou notre race. Qu'il veut établir un véritable rapport d'intimité avec nous, dans une relation de confiance et de fidélité.  
Aide-moi à ouvrir mon coeur à sa présence en lui donnant toute la place et ainsi le suivre fidèlement tout au long de ma vie.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 6ème jour

{{< hint info >}}
"En vérité, en vérité, je vous le dis, un serviteur n'est pas plus grand que son maître, ni un envoyé plus grand que celui qui l'envoie. Sachant cela, vous serez heureux si du moins vous le mettez en pratique." (Jean 13:16)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus nous met en garde contre les faux bonheurs: ceux de désirer d'être le premier, de vouloir toujours plus de pouvoir sur les autres, de dominer; d'être constamment à la recherche des honneurs et des flatteries qui conduisent malheureusement à l'orgueil et à l'égoïsme.  
Aide-moi à me faire comprendre que lorsque mes désirs se limitent à cela, je demeure toujours profondément insatisfaits et inassouvis.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 7ème jour

{{< hint info >}}
**Jésus dit:**  
"Je suis le chemin et la vérité et la vie. Personne ne va au Père si ce n'est pas par moi." (Jean 14:6)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que le Christ est l'unique chemin qui conduit à Dieu, qu'il est la vérité et la vie que Dieu nous a léguées.  
L'unique chemin qui conduit au véritable bonheur.  
Souvent je suis porté à choisir le chemin de la facilité, à me laisser influencer par des faux dieux, l'argent, les biens matériels qui deviennent ma première priorité.  
Aide-moi à comprendre que ce ne sont que des joies éphémères et que c'est le Christ qui me conduit vraiment au bonheur promis.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 8ème jour

{{< hint info >}}
Cependant Thomas, l'un des douze, n'était pas avec les autres disciples lorsque Jésus vint.  
**Les autres disciples lui dirent donc:** "Nous avons vu le Seigneur!"  
**Mais il leur répondit:** "si je ne vois pas dans ses mains la marque des clous, si je n'enfonce pas mon doigt à la place des clous et si je n'enfonce pas ma main dans son côté, je ne croirai pas!"...  
**Jésus vint, toutes les portes verrouillées, il se tient au milieu d'eux et leur dit:**  
"Avance ton doigt ici et regarde mes mains; avance ta main et enfonce-la dans mon côté, cesse d'être incrédule et deviens un homme de foi."  
**Thomas lui répondit:** "Mon Seigneur et mon Dieu,"  
**Jésus dit:** "Parce que tu m'as vu, tu as cru; bienheureux ceux qui, sans avoir vu, ont cru." (Jean 20:24-26,27-29)  
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus est vraiment ressuscité, que la vie a triomphé de la mort et que l'amour a vaincu la haine.  
Aide-moi à enlever dans mon esprit tout ce qui me fait douter.  
Aide-moi à faire de moi un véritable témoin de la résurrection: voir, croire, non pas voir pour croire, mais voir et croire.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.

## 9ème jour

{{< hint info >}}
Si vous m'aimez, vous vous appliquerez à observer mes commandements, moi je prierai le Père: il vous donnera un autre Paraclet qui restera avec vous pour toujours.  
C'est lui l'Esprit de vérité... Je ne vous laisserai pas orphelins, je viens à vous. (Jean 14:15;17.18)
{{< /hint >}}

Merci Jean, de me rappeler par ton Évangile que Jésus, ne nous abandonnera jamais, qu'à travers mes difficultés et mes souffrances son Esprit est toujours présent.  
Cet Esprit Saint, le Paraclet qui est l'intercesseur, le défenseur et le consolateur.  
Et que c'est par cet Esprit que je peux reconnaître l'amour de Dieu, l'Amour qui sauve, qui console et qui guérit.

Jean, toi l'apôtre bien-aimé, toi qui fus si près de Jésus, aujourd'hui je mets en toi toute ma confiance et plein d'espoir, je m'adresse à toi en te demandant en toute humilité d'intercéder auprès de Lui, afin qu'il m'obtienne la demande que je sollicite (...).

Amen.
