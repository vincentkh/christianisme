# Saint Joseph, père dans la tendresse

Saint Joseph, père dans la tendresse,  
apprends nous à accepter d’être aimés précisément dans ce qui en nous est plus faible.  
Accorde-nous de ne placer aucun obstacle entre notre pauvreté et la grandeur de l’amour de Dieu.  
Suscite en nous le désir de nous approcher du Sacrement de la Réconciliation, pour être pardonnés et aussi rendus capables d’aimer avec tendresse nos frères et sœurs dans leur pauvreté.
Sois proche de ceux qui ont fait le mal et qui en paient le prix;  
Aide-les à trouver ensemble avec la justice également la tendresse pour pouvoir recommencer.  
Et apprends leur que la première manière de recommencer est de demander sincèrement pardon.  
Amen.
