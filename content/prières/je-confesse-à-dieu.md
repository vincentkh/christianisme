# Je confesse à Dieu

Je confesse à Dieu tout-puissant, Je reconnais devant mes frères que j’ai péché, en pensée, en parole, par action et par omission.  
Oui, j’ai vraiment péché.  
C’est pourquoi, je supplie la Vierge Marie, les anges et tous les saints, et vous aussi, mes frères, de prier pour moi le Seigneur notre Dieu.
