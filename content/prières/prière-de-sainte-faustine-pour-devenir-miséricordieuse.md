# Prière de Sainte Faustine pour devenir miséricordieuse

## Sainte Faustine (Petit Journal n° 163)

Je désire me transformer tout entier en Ta miséricorde et être ainsi un vivant reflet de Toi, ô Seigneur;
que le plus grand des attributs divins, Ton insondable miséricorde, passe par mon âme et mon cœur sur le prochain.

Aide-moi, Seigneur, pour que mes yeux soient miséricordieux, pour que je ne soupçonne jamais ni ne juge d’après les apparences extérieures, mais que je discerne la beauté dans l’âme de mon prochain et que je lui vienne en aide.

Aide-moi, Seigneur, pour que mon oreille soit miséricordieuse, afin que je me penche sur les besoins de mon prochain et ne reste pas indifférent à ses douleurs ni à ses plaintes.

Aide-moi, Seigneur, pour que ma langue soit miséricordieuse, afin que je ne dise jamais de mal de mon prochain, mais que j’aie pour chacun un mot de consolation et de pardon.

Aide-moi Seigneur, pour que mes mains soient miséricordieuses et remplies de bonnes actions, afin que je sache faire du bien à mon prochain et prendre sur moi les tâches les plus lourdes et les plus déplaisantes.

Aide-moi, Seigneur, pour que mes pieds soient miséricordieux, pour me hâter au secours de mon prochain, en dominant ma propre fatigue et ma lassitude. Mon véritable repos est de rendre service à mon prochain.

Aide-moi, Seigneur, pour que mon coeur soit miséricordieux, afin que je ressente toutes les souffrances de mon prochain. Je ne refuserai mon coeur à personne. Je fréquenterai sincèrement même ceux qui, je le sais, vont abuser de ma bonté, et moi, je m’enfermerai dans le Cœur très miséricordieux de Jésus. Je tairai mes propres souffrances.

Que Ta miséricorde repose en moi, ô mon Seigneur.
C’est toi qui m’ordonnes de m’exercer aux trois degrés de la miséricorde ; le premier : l’acte miséricordieux – quel qu’il soit ; le second : la parole miséricordieuse – si je ne puis aider par l’action, j’aiderai par la parole ; le troisième- c’est la prière. Si je ne peux témoigner la miséricorde ni par l’action, ni par la parole, je le pourrai toujours par la prière. J’envoie ma prière même là où je ne puis aller physiquement.

Ô mon Jésus, transforme-moi en Toi, car Tu peux tout.
