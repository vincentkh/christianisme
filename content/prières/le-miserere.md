# Le Miserere

## Psaume 51 (50)

Pitié pour moi, Dieu, en ta bonté, en ta grande tendresse efface mon péché.  
Lave-moi tout entier de mon mal et de ma faute purifie-moi.  
Car mon péché, moi, je le connais, ma faute est devant moi sans relâche ; contre toi, toi seul, j’ai péché, ce qui est mal à tes yeux, je l’ai fait.
Pour que tu montres ta justice quand tu parles et que paraisse ta victoire quand tu juges.  
Vois, mauvais je suis né, pécheur ma mère m’a conçu.  
Mais tu aimes la vérité au fond de l'être, dans le secret tu m'enseignes la sagesse.  
Ote mes taches avec l'hysope, je serai pur; lave-moi, je serai blanc plus que neige.  
Rends-moi le son de la joie et de la fête.  
Qu'ils dansent, les os que tu broyas!  
Détourne ta face de mes fautes, et tout mon mal, efface-le.  
Dieu, crée pour moi un cœur pur, restaure en ma poitrine un esprit ferme; ne me repousse pas loin de ta face, ne m'enlève pas ton esprit de sainteté.
Rends-moi la joie de ton salut, assure en moi un esprit magnanime.  
Aux pécheurs j'enseignerai tes voies, à toi se rendront les égarés.  
Affranchis-moi du sang, Dieu, Dieu de mon salut, et ma langue acclamera ta justice;  
Seigneur, ouvre mes lèvres, et ma bouche publiera ta louange.  
Car tu ne prends aucun plaisir au sacrifice; un holocauste, tu n'en veux pas.  
Le sacrifice à Dieu, c'est un esprit brisé; d'un cœur brisé, broyé, Dieu, tu n'as point de mépris.  
En ton bon vouloir, fais du bien à Sion rebâtis les remparts de Jérusalem!  
Alors tu te plairas aux sacrifices de justice, holocauste et totale oblation, alors on offrira de jeunes taureaux sur ton autel.
