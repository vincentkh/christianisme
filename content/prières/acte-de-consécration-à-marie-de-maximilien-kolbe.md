# Acte de consécration à Marie de Maximilien Kolbe

Daigne recevoir ma louange, ô Vierge bénie ! 
Immaculée Conception, Reine du Ciel et de la Terre, 
Refuge des pécheurs et Mère très aimante 
à qui Dieu voulut confier tout l’ordre de la miséricorde.

Me voici à tes pieds, moi ..., pauvre pécheur.

Je t’en supplie, accepte mon être tout entier 
comme ton bien et ta propriété.
Agis en moi selon ta volonté, en mon âme et mon corps, 
en ma vie et ma mort et mon éternité.
Dispose avant tout de moi comme tu le désires,
pour que se réalise enfin ce qui est dit de toi :
« La Femme écrasera la tête du serpent. » Et aussi :
« Toi seule vaincras les hérésies dans le monde entier. »

Qu’en tes mains toutes pures, si riches de miséricorde,
je devienne un instrument de ton amour
capable de ranimer et d’épanouir pleinement
tant d’âmes tièdes ou égarées.
Ainsi s’étendra sans fin le règne du Cœur divin de Jésus.

Vraiment ta seule présence attire les grâces
qui convertissent et sanctifient les âmes,
puisque la grâce jaillit du Cœur divin de Jésus 
sur nous tous en passant par tes mains maternelles.

Amen.
