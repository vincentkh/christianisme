# Ô sainte et miraculeuse dame de Chartres

![Notre dame de Chartres](/christianisme/notre-dame-de-chartres-xs.jpg)

Ô sainte et miraculeuse dame de Chartres, souvenez-vous que votre fils bien-aimé, durant son passage sur la terre, ne se refusa jamais à user de sa toute-puissance pour guérir les malades, les infirmes que l'on apportait à ses pieds.  
Moi aussi, rempli de confiance en votre bonté, je viens auprès de vous, languissant et malade, comme un enfant auprès de sa mère, pour demander du soulagement, et je vous conjure, ô mère du Dieu sauveur, de vouloir bien user, en ma faveur, de ce pouvoir admirable dont votre fils vous a revêtue.  
J'ose vous demander la santé, comme un bien nécessaire à l'accomplissement de mes devoirs sur la terre, et mettant en vous seule ma confiance, j'attends ma guérison de votre maternelle bonté.  
Mais si la volonté de Dieu exigeait que je le servisse au milieu d'une langueur continuelle, je soumets mes vœux à ses adorables desseins, et je vous prie alors, ô Marie, de daigner m'obtenir l'esprit patient, pénitent, intérieur, dont j'ai besoin pour unir mes souffrances à Celles de Jésus, afin que cette croix devienne pour moi la voie du salut et des bénédictions éternelles.

Ainsi soit-il.
