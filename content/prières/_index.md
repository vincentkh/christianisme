# Prières

[Le Miserere]({{< relref "/prières/le-miserere" >}})  
[Je confesse à Dieu]({{< relref "/prières/je-confesse-à-dieu">}})  
[Saint ! Saint ! Saint !]({{< relref "/prières/saint-saint-saint">}})  
[Communion spirituelle]({{< relref "/prières/communion-spirituelle">}})  
[Mon Seigneur et mon Dieu]({{< relref "/prières/mon-seigneur-et-mon-dieu">}})  
[Je vous salue, je vous adore et je vous aime]({{< relref "/prières/je-vous-salue-je-vous-adore-et-je-vous-aime">}})  
[Prière des Servantes de l’Eucharistie]({{< relref "/prières/prière-des-servantes-de-l-eucharistie">}})  
[Je T’adore profondément, divinité cachée]({{< relref "/prières/je-t-adore-profondément-divinité-cachée">}})

- Le Credo

  - [Le Credo - Symbole des Apôtres]({{< relref "/prières/le-credo-symbole-des-apôtres">}})
  - [Le Credo de Nicée-Constantinople]({{< relref "/prières/le-credo-de-nicée-constantinople">}})

- Notre Dame

  - [Salve Regina]({{< relref "/prières/salve-regina" >}})
  - [Ô sainte et miraculeuse dame de Chartres]({{< relref "/prières/ô-sainte-et-miraculeuse-dame-de-chartres">}})
  - [Prière de Saint Alphonse de Liguori]({{< relref "/prières/prière-de-saint-alphonse-de-liguori" >}})
  - [Acte de consécration à Marie de Maximilien Kolbe]({{< relref "/prières/acte-de-consécration-à-marie-de-maximilien-kolbe" >}})
  - [Prière de carême du père Jean-Paul Hoch]({{< relref "/prières/prière-de-carême-du-père-jean-paul-hoch">}})

- Saint Joseph

  - [Saint Joseph, père dans la tendresse]({{< relref "/prières/saint-joseph-père-dans-la-tendresse">}})
  - [Je vous salue Joseph]({{< relref "/prières/je-vous-salue-joseph">}})
  - [Ô glorieux saint Joseph]({{< relref "/prières/ô-glorieux-saint-joseph">}})

- Saint Michel

  - [Prière du Pape Léon XIII à Saint Michel Archange]({{< relref "/prières/prière-du-pape-léon-xiii-à-saint-michel-archange">}})
  - [Grand Prince de la milice céleste]({{< relref "/prières/grand-prince-de-la-milice-céleste">}})

- Sainte Thérèse

  - [Ô sainte Thérèse de l'Enfant-Jésus]({{< relref "/prières/ô-sainte-thérèse-de-l'enfant-jésus">}})

- Sainte Faustine

  - [Prière de Sainte Faustine pour devenir miséricordieuse]({{< relref "/prières/prière-de-sainte-faustine-pour-devenir-miséricordieuse">}})

- Sainte Jeanne d'Arc

  - [Par ta puissante intercession de Sainte Jeanne d'Arc]({{< relref "/prières/par-ta-puissante-intercession-de-sainte-jeanne-d-arc">}})

- Saint François d’Assise

  - [Prière de Saint François d’Assise]({{< relref "/prières/prière-de-saint-françois-d-assise">}})

- Sainte Rita

  - [Ô glorieuse Sainte Rita]({{< relref "/prières/ô-glorieuse-sainte-rita">}})

- Sainte Brigitte de Suède

  - [The ninth prayer of Saint Bridget of Sweden]({{< relref "/prières/the-ninth-prayer-of-saint-bridget-of-sweden">}})

- Sainte Maria Valtorta

  - [Père, aide-moi à me sanctifier]({{< relref "/prières/père-aide-moi-à-me-sanctifier">}})

- Vénérable Marcel Van

  - [Prière pour la France]({{< relref "/prières/prière-pour-la-france">}})

- Anglais
  - [Apostles Creed]({{< relref "/prières/apostles-creed">}})
  - [Saint Francis Vocation Prayer]({{< relref "/prières/saint-francis-vocation-prayer">}})
