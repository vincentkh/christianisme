# Prière du Pape Léon XIII à Saint Michel Archange (1884)

Saint Michel Archange, défendez-nous dans le combat, soyez notre soutien contre la perfidie et les embûches du démon.  
Que Dieu exerce sur lui son empire, nous le demandons en suppliant, et vous, prince de la milice céleste, refoulez en enfer, par la vertu divine, Satan et les autres esprits mauvais qui errent dans le monde pour la perte des âmes.

Amen.
