# Prière pour la France

« Seigneur Jésus, aie compassion de la France, daigne l'étreindre dans ton Amour et lui en montrer toute la tendresse.  
Fais que, remplie d'Amour pour toi, elle contribue à te faire aimer de toutes les nations de la Terre.  
Ô Amour de Jésus, nous prenons ici l'engagement de te rester à jamais fidèles et de travailler d'un cœur ardent à répandre ton Règne dans tout l'univers.  
Amen. »

Vénérable Marcel Van (1928-1959)  
Religieux rédemptoriste vietnamien (Son procès de béatification est ouvert)  
Extrait du bulletin des Amis de Van numéro 82 (octobre 2021) page 9  
Dans le livre des Colloques, Marcel Van affirme que Jésus-Christ lui a communiqué une « prière pour la France… sortie directement de son Cœur ».
