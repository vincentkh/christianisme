# Prière de Saint Alphonse de Liguori

Ô ma douce Souveraine et Mère ! Je vous aime, et parce que je vous aime, j’aime aussi votre saint nom.  
Je me propose et j’espère avec votre secours de l’invoquer toujours pendant ma vie et à ma mort.  
Pour la gloire de votre nom, lorsque mon âme sortira de ce monde, venez à sa rencontre, et recevez-la dans vos bras.  
Ne dédaignez pas, ô Marie, de venir la consoler alors par votre douce présence.  
Soyez pour elle l’échelle et le chemin du paradis.  
Obtenez-lui la grâce du pardon et de l’éternel repos.  
Ô Marie ! Notre avocate, c’est à vous de défendre vos serviteurs, et de plaider leur cause au tribunal de Jésus-Christ.
