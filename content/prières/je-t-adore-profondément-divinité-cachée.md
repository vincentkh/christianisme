# Je T’adore profondément, divinité cachée

Je T’adore profondément, divinité cachée,  
vraiment présente sous ces apparences;  
à Toi mon coeur se soumet tout entier  
parce qu’à Te comtempler, tout entier il défaille.  
La vue, le goût, le toucher ne T’atteignent pas:  
à ce qu’on entend dire seulement il faut se fier;  
je crois ce qu’a dit le Fils de Dieu;  
rien de plus vrai que cette parole de Vérité.

Catéchèse de l’église catholique
