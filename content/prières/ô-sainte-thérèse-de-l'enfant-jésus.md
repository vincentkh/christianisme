# Ô sainte Thérèse de l'Enfant-Jésus

Ô sainte Thérèse de l'Enfant-Jésus,  
qui avez mérité d'être proclamée Patronne des missions Catholiques du monde entier,  
souvenez-vous du très ardent désir que vous avez manifesté ici-bas de planter la Croix de Jésus-Christ sur tous les rivages et d'annoncer l'Évangile jusqu'à la consommation des siècles.  
Aidez, nous vous en prions, selon votre promesse, les prêtres, les missionnaires, toute l'Église.  
Ô sainte Thérèse de l'Enfant-Jésus, patronne des Missions, priez pour nous.
