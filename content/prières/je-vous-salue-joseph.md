# Je vous salue Joseph

Je vous salue Joseph,  
Vous que la grâce divine a comblé.  
Le sauveur a reposé entre vos bras et grandi sous vos yeux.  
Vous êtes béni entre tous les hommes et Jésus, l’enfant divin de votre virginale épouse est béni.  
Saint Joseph, donné pour père au Fils de Dieu, priez pour nous dans nos soucis de famille, de santé et de travail, jusqu’à nos derniers jours, et daignez nous secourir à l’heure de notre mort.
