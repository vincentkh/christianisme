# Je vous salue, je vous adore et je vous aime

Je vous salue, je vous adore et je vous aime, ô Jesus mon Sauveur, couvert de nouveaux outrages par les blasphémateurs, et je vous offre, dans le Coeur de la divine Marie, comme un encens et un parfum d’agréable odeur, les hommages des Anges et de tous les saints, en vous priant humblement par la vertu de votre Sainte Face, de réparer et de rétablir en moi et dans tous les hommes, votre image défigurée par le péché.  
Ainsi soit il.
