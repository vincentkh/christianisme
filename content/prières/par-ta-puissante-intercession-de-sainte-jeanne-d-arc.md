# Par ta puissante intercession de Sainte Jeanne d'Arc

Par ta puissante intercession de Sainte Jeanne d'Arc, patronne de la France en second après ta très Sainte Mère, nous te supplions, ô Jésus: répands sur ce pays qui proclamait jadis en préambule de ses lois "Vive le Christ qui est Roi des Francs!" de nouvelles et abondantes grâces de conversion et de Foi, pour que les coeurs et les esprits reviennent à toi!
