# Prière des Servantes de l’Eucharistie

Ô Jésus qui êtes réellement présent dans l’Eucharistie, je joins mon cœur à votre Cœur adorable, immolé en perpétuel sacrifice sur tous les autels du monde, louant le Père et implorant la venue de votre Règne, et je vous fais l’oblation totale de mon corps et de mon âme.  
Daignez agréer cette humble offrande comme il vous plaira, pour la gloire de Dieu et le salut des âmes.  
Sainte Mère du Ciel, ne permettez pas que je sois séparé de votre divin Fils, et gardez-moi toujours comme votre propriété.

Amen.
