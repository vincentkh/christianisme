# Prière de carême du père Jean-Paul Hoch

Voici une prière de carême du père Jean-Paul Hoch, prêtre pendant dix ans en République Centrafricaine (1978-1988) et Supérieur Général de la Congrégation du Saint-Esprit de 2004 à 2012, pour nous aider à dire « oui » en toutes circonstances :

« Quand vient pour nous l’heure de la décision, Marie de l’Annonciation, aide-nous à dire “oui” quand vient pour nous l’heure du départ;  
Marie d’Égypte, épouse de Joseph, allume en nous l’Espérance.  
Quand vient pour nous l’heure de l’incompréhension, Marie de Jérusalem, creuse en nous la patience.  
Quand vient pour nous l’heure de l’intervention, Marie de Cana, donne-nous le courage de l’humble parole.  
Quand vient pour nous l’heure de la souffrance, Marie du Golgotha, fais-nous rester aux pieds de ceux en qui souffre ton Fils.  
Quand vient pour nous l’heure de l’attente, Marie du Cénacle, inspire-nous une commune prière.  
Et chaque jour, quand sonne pour nous l’heure joyeuse du service, Marie de Nazareth, Marie des Monts de Juda, mets en nous ton cœur de servante.  
Jusqu’au jour où, prenant ta main, Marie de l’Assomption, nous nous endormirons, dans l’attente du jour de notre résurrection. »
