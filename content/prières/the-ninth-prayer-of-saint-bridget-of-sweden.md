# The ninth prayer of Saint Bridget of Sweden

Our Father ... Hail Mary ...

O Jesus! Royal virtue, joy of the mind, recall the pain Thou didst endure when, plunged in an ocean of bitterness at the approach of death, insulted, outraged by the Jews, Thou didst cry out in a loud voice that Thou was abandoned by Thy Father, saying:  
"My God, My God, why hast Thou forsaken me?"  
Through this anguish, I beg of Thee, O my Saviour, not to abandon me in the terrors and pains of my death.

Amen.
