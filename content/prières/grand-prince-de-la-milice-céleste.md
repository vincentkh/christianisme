# Grand Prince de la milice céleste

![Saint Michel](/christianisme/saint-michel.webp)

Grand Prince de la milice céleste, établi par la Providence Divine le protecteur spécial de la France, de grâce ne transportez pas à une autre nation le glorieux privilège de vous avoir pour ange tutélaire.  
Ah ! Ne souffrez pas que notre patrie cesse d’être la fille aînée de l’Église et que son glorieux titre passe à un autre peuple !  
Opposez à ses défaillances actuelles la fidélité séculaire de son passé.  
Souvenez-vous que cette terre confiée à votre sollicitude, fécondée par les sueurs et le sang de nombreux apôtres et martyrs, fut illustre entre toutes par les vertus de ses enfants, depuis Saint Germain et Sainte Geneviève jusqu’aux âmes généreuses qui, de nos jours encore, réagissent contre les envahissements du mal par l’énergie de leur foi et la sainteté de leurs oeuvres.  
Ô glorieux Archange, faites-vous notre avocat devant le Très-Haut.  
Obtenez pour la France, notre chère patrie, la paix dont elle a tant besoin à l’intérieur et à l’extérieur.  
Obtenez-lui un prompt et sincère retour à l’antique foi, source de sa force et de sa grandeur, afin qu’après avoir été humiliée sous les châtiments du Ciel pour ses fautes, elle se relève purifiée et retrempée, capable des mâles vertus qui ont fait sa gloire dans les siècles passés.

Ainsi soit-il.

(Prière indulgenciée)
