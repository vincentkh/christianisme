# Père, aide-moi à me sanctifier.

Père, aide-moi à me sanctifier.  
Ma faiblesse a besoin de toi pour être forte.  
Père, je veux t’aimer parfaitement, et je ne sais pas le faire.  
Apprends-le-moi, Toi, l’Amour.  
Père, je sais et je me rappelle ce que tu m’as déjà dit.  
Sans toi, je serai misérable physiquement et, plus encore, spirituellement.  
Merci de tout, Père.  
Je te dis: “Continue, continue Tes bienfaits.”  
Mais ce n’est pas par soif de bien-être humain.  
Plus que pour mon corps, je te dis “encore” pour mon âme, à laquelle je veux rendre la Patrie éternelle.  
Père saint, ta petite créature aspire à ton sein.  
Soutiens-moi sur le chemin afin que je ne dévie pas vers d’autres voies mais que je parvienne à Toi, mon Repos et ma Joie.

Les Cahiers - 26 Juillet 1944 - Maria Valtorta
