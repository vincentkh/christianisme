# Ô glorieuse Sainte Rita

Ô glorieuse Sainte Rita, toi qui es dans le ciel une protectrice puissante auprès de Dieu, nous nous tournons vers toi avec confiance et abandon.  
Interviens auprès du Seigneur tout-puissant et bon.  
Aide-nous auprès de Dieu.  
C’est sur ton intercession que nous avons basé notre confiance: couronne nos espoirs, entends nos prières.  
Ô Dieu éternel dont la miséricorde est sans mesure et la bonté infinie, nous Te rendons grâce pour les bienfaits que Tu nous accorde par les mérites de Sainte Rita.  
Ô toi qui est si bonne, Sainte Rita, fais que nous devenions de plus en plus dignes de la miséricorde de Dieu et de ta protection.

Amen.
