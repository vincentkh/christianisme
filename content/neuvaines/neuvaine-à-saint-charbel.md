# Neuvaine à Saint Charbel

## Introduction

Youssef Makhlouf nait le 8 mai 1828 à la maison de son grand-père maternel qui est située dans un petit village du Liban.
Il a deux frères et deux soeurs et il est le cadet de la famille.

Son père est laboureur, un paysan, vivant de la culture.
Sa mère s'occupe du quotidien de la maison.
Ses parents sont très croyants et il hérite ainsi d'une foi très ardente et intense.

Dès l'âge de vingt ans, saint Charbel, est attiré par la vie monastique et se retrouve au monastère de Notre-Dame de Maïfouk.

En 1853, Youssef devient moine, il prend le nom de Charbel et en 1859 il est ordonné prêtre.
En 1875 il se retire dans l'ermitage voisin du monastère.
Il est un moine exemplaire dans l'observation des règles.

En décembre 1898 durant la messe, Charbel se retrouve subitement paralysé.
Et dans les 8 jours qui précède sa mort, il continue de prier et à observer les Règles, le 24 décembre il rend l'âme.

## Biographie

| Nom complet   | Youssef Antoun Makhlouf (Charbel Makhlouf), Moine et prêtre de l'ordre maronite                        |
| ------------- | ------------------------------------------------------------------------------------------------------ |
| Béatification | 5 décembre 1965                                                                                        |
| Canonisation  | 9 octobre 1977                                                                                         |
| Par           | Pape Paul VI                                                                                           |
| Endroit       | Basilique Saint-Pierre de Rome                                                                         |
| Fête          | 24 juillet (rite romain), 24 décembre (dans sa région), 3ème dimanche de juillet (calendrier maronite) |
| Vénéré par    | Église catholique, Église maronite, Église catholique syriaque, Églises catholiques orientales         |
| Patron        | Saint Patron du Liban                                                                                  |
| Tombeau       | Annaya, Liban                                                                                          |

## Paroles de saint Charbel

{{< hint info >}}
"... J'étais en présence d'un Être suprême, irisé de lumière. Je sentais que j'étais enveloppé d'un amour immense de endresse et en même temps, en communion avec tout l'univers, comme si je faisais un avec le cosmos..." (1)
{{< /hint >}}

(1) Réf. Saint Charbel ... selon ses contemporains, Père Hanna Skandar, Les publications du couvent Notre Dame du fort, p 138

## Neuvaine à Saint Charbel

Une neuvaine à Saint Charbel est une occasion de plonger dans l'univers intérieur d'une personne qui a marché avec Dieu quotidiennement et qui s'est abandonné à Lui.

Saint Charbel a su à travers les étapes de sa vie, renoncer à lui-même, pour devenir un serviteur de Dieu, à part entière.
Il nous permet de mieux découvrir ce chemin intérieur qui nous guide vers le monde de l'invisibile, là où nous goûtons à part entière, le grand amour de Dieu pour nous et pour l'humanité.

Nous ne pouvons fréquenter ce chemin réellement que si notre décision est teintée d'une soif de liberté et d'une quête de la vérité.

Saint Charbel, est le saint patron des libanais chrétiens.
Il a été un moine très priant qui nous invite à toujours garder confiance en Dieu et à compter non pas simplement sur nous, mais sur la force et la lumière que Dieu donne généreusement à ses enfants.

Cette neuvaine réalisée sur les pas de Saint Charbel, cette demande de prière, nous permettra de mieux découvrir les bontés et la fidélité de notre Dieu.

Que ce temps de fois nous aide à mieux grandir chaque jour, comme fils et fille de la Lumière, à la suite de Jésus Christ, notre lumière et notre salut !

## Demandes

Saint Charbel, toi qui as pris le chemin de l'intérieur pour goûter pleinement à l'amour et à la tendresse du Père afin de chercher à faire sa volonté.
Aide-nous à nous à trouver et à nous centrer sur l'essentiel, sur ce qui fait grandir en humanité et nous permet de devenir davantage chaque jour, des véritables témoins du Christ.

Apprends-nous à goûter à la vie sans nous détourner de ce qui est vrai: aimer le Seigneur de tout notre coeur, de toute notre force et aimer notre prochain comme soi-même.

Tu as renoncé à toi-même pour suivre le Christ, apprends-nous à faire les bons choix, à discerner et à rechercher avec ardeur la lumière et la vérité.

Tu as donné à Dieu, la première place dans ton coeur et dans ta vie, présentes aurjoud'hui en mon nom cette demande (...) à Dieu et aide-moi à chercher sans cesse sa volonté et à m'abandonner à lui.

1 Notre Père, 10 Je vous salue Marie, 1 Gloire au Père

## L'Amour et la Chartié

Saint Charbel, tu nous rappelles que le fondement de toutes choses, c'est l'amour et la vérité, qu'il n'y a pas de vérité sans amour.
Dieu est le véritable Amour et le Chemin qui nous y mène est le Christ, il est la vérité et l'amour incarné.
"Je suis le Chemin, la Vérité et la Vie" (Jean 14:6)

Mais pour découvrir ce Chemin d'amour, il nous faut prier. Car la prière nous conduit à sa source authentique, la seule, la vraie qui est le Fils de Dieu, Jésus-Christ, amour incarné, par l'Esprit-Saint.

Dieu nous aime gratuitement et nous demande de l'aimer et de nous aimer les uns les autres, c'est le commandement le plus important "l'essentiel".
"Tu aimeras le Seigneur, ton Dieu, de tout ton coeur, de toute ton âme et de toute ta pensée. C'est là le commandement le plus grand et le plus important. Et il y en a un second qui lui est semblable: tu aimeras ton prochain comme toi-même"

Dieu est Amour, il nous aime sans conditions et sans limites, par notre baptême, nous sommes devenues ses enfants, car nous sommes nés du Coeur de Dieu et au Coeur de Dieu nous y retournerons.

## 1er jour - Une vie sainte

Saint Charbel nous a démontré que les vrais trésors sont du domaine de l'âme et du coeur.
Bien sûr, le confort et le bien-être nous permettent d'apprécier la vie.
Mais, il y a plus que cela... il y a ce chemin le moins fréquenté en notre monde, le chemin de l'intérieur où nous rencontrons nos vrais désirs, nos vrais besoins.
Une vie sainte est une recherche du véritable amour qui doit habiter nos pensées, nos gestes, nos décisions dans le quotidien de nos vies.

{{< hint info >}}
**Jésus nous dit:**  
"Je suis la lumière du monde; celui qui me suit ne marchera pas dans les ténèbres, mais il aura la lumière de la vie" (Jn 8:12)
{{< /hint >}}

Ce désir de grandir chaque jour comme enfant de Dieu, d'accomplir la promesse de notre Baptême, d'être en relation, en dialogue avec Dieu à chacun de nos pas et de faire de notre vie, une histoire d'amour, à la suite de Jésus et sous le souffle de l'Esprit.

Demandons aujourd'hui à saint Charbel de nous aider à trouver au fond de notre coeur, le chemin qui conduit à la sainteté, nous guide à vraie lumière et qui nous amène sur la route de l'amour et de la charité.

## 2ème jour - À l'écoute de Dieu

Il y a en notre monde tant de bruits et de distractions extérieures; il n'est pas facile de s'arrêter et de prendre du temps pour soi, du temps de recueillement et de prière, de faire silence.

{{< hint info >}}
**Mère Teresa**  
"Dieu est l'ami du silence. Les arbres, les fleurs et l'herbe poussent en silence. Regarde les étoiles, la lune et le soleil, comment ils se meuvent silencieusement."
{{< /hint >}}

Le silence n'est pas un simple vide, il est habité par Dieu et à travers nos silences, Dieu nous accompagne, il nous écoute, il nous parle et nous répond.

C'est par la méditation, l'oraison, la prière personnelle et communautaire que Dieu nous rejoint et qu'il manifeste son amour.
Lorsque dans le Notre Père, Jésus dit: "Donne-nous le pain de ce jour", il ne nous parle pas simplement des besoins matériels, de la nourriture, il nous montre aussi le chemin qui nous permet de vivre et de traverser les joies et les peines, les réussites et les échecs.

Demandons aujourd'hui à saint Charbel de nous apprendre à s'arrêter pour faire silence, pour être à l'écoute de Dieu qui nous guide à travers de ce que nous vivons.

## 3ème jour - Savoir donner

Il est très humain de vouloir posséder, avoir, acquérir, assurer son avenir, sa sécurité financière.
Lors des tentations au désert, Jésus a refusé le pouvoir, la gloire et la richesse en nous rappelant que l'essentiel, le plus grand amour, le vrai bonheur est de donner sa vie pour ceux qu'on aime.
"Il n'y a pas de plus grand amour que de donner sa vie pour ses amis" (Jean 15:13)

Une personne qui met dans sa vie comme priorité le chacun de soi, la réussite personnelle et même l'égoïsme et l'égocentrisme, ne trouvera jamais le vrai bonheur, car le véritable bonheur ne se trouve pas dans la gloire ou dans les biens engrangés, mais à travers la charité et le don de soi.

{{< hint info >}}
**Jésus nous dit:**  
"Là où est ton trésor, là aussi sera ton coeur" (Matthieu 6:21)
{{< /hint >}}

Le Christ nous appelle à donner gratuitement chaque jour à donner sans attendre en retour, et ce en toute simplicité, mettre ses talents, son énergie au service des plus petits, des pauvres et des malheureux.

Demandons aujourd'hui à saint Charbel de nous aider à mettre de côté nos égoïsmes, afin de goûter au vrai bonheur de la vie, celui d'être au service des autres.

## 4ème jour - L'humilité vers la liberté

Il y a mille et une manière de vivre en notre époque.
On valorise les succès, les réussites, et les performances.

Mais quel est le but de notre vie, est-ce avant tout, de rechercher la gloire, les honneurs et la popularité? ou plutôt mettre nos talents, nos compétences et notre temps au service de notre prochain, des plus petits.

Se mettre au service de l'autre, c'est tout d'abord répondre aux demandes de Dieu.
Comment? En posant d'abord sur lui son regard, le regard de Dieu.
C'est-à-dire, un regard plein d'amour, de tendresse et de compassion, c'est travailler à établir le royaume des Cieux.

{{< hint info >}}
**Saint Élizabeth de la Trinité**  
"Comment satisfaire les besoins du regard de Dieu, sinon en se tenant simplement et amoureusement tourné vers lui afin qu'il puisse refléter sa propre image, comme le soleil se reflète au travers d'un pur cristal ?"
{{< /hint >}}

Penser et aider les plus petits, nous aidera à être heureux, se sentir utile en toute modestie et humilité.
Il nous libérera ainsi de nos chaînes qui nous tiennent prisonnier: orgueil, vanité, ingratitude.
Il nous rendra enfin libre.

## 5ème jour - Renoncer à soi-même

Chacun de nous porte parfois des grands rêves et des grandes ambitions dans la vie.
Parfois, on avance dans ces chemins sans écouter notre coeur, guidé par la soif de la réussite et de la performance.

Chaque être humain est unique et aimé de Dieu, il est important de réfléchir et de rechercher la volonté de Dieu.
À quoi Dieu nous appelle-t-il vraiment?

{{< hint info >}}
"Si quelqu'un veut venir après moi, qu'il renonce à lui-même, qu'il se charge chaque jour de sa croix, et qu'il me suive. Car celui qui voudra sauver sa vie la perdra, mais celui qui la perdra à cause de moi la sauvera" (Luc 9:23-24)
{{< /hint >}}

Pour être à la suite de Jésus, la consigne est claire: rechercher le chemin de l'amour à la suite de Jésus, accepter Jésus comme Maître, Guide et Sauveur.
Voilà le chemin du renoncement où nous pouvons chaque jour avancer où Dieu nous appelle et nous attend.

Demandons aujourd'hui à saint Charbel de nous aider à écouter l'appel de Dieu, afin d'être à sa suite et faire sa volonté.

## 6ème jour - En communion avec Dieu

Dans notre vie, ils nous arrive de faire l'expérience de la solidarité.
Nous apprenons à travers des évènements à vivre l'appartenance et l'entraide.
Et pour ne pas avancer seul, il nous faut être à l'écoute des autres, attentifs à leurs paroles et à leurs attentes.

Nous apprenons au fil des jours à avoir un coeur commun.
C'est ce qui devrait être le but de toute famille, vivre les uns avec les autres dans l'écoute, l'accueil, le soutien et la confiance.

L'Église est la grande famille de Dieu où l'on apprend comme chrétiens et chrétiennes, l'expérience de la communion par la solidarité, la justice, la prière, l'accueil et la mise en pratique de la Parole de Dieu.

Par l'Eucharistie, nous vivons l'expérience de la communion, Dieu vient nous nourrir, habiter en nous par sa Parole et par le Pain de Vie.

{{< hint info >}}
**Jésus dit:**  
"Le Père est en moi et moi je suis dans le Père" (Jean 14:11)
{{< /hint >}}

La communion c'est avoir un coeur débordant de la présence de Dieu qui nous donne son amour.

Demandons aujourd'hui à saint Charbel de nous aider à être des chrétiens et chrétiennes solidaires, grandir en se nourrissant du Corps et du Sang du Christ.

## 7ème jour - La force de la prière

Souvent, nous ne trouvons pas le temps de prier.
La prière du matin, le bénédicité, la prière du soir, le chapelet, ne sont plus des moments privilégiés.
Mais comment faire pour garder notre foi vivante afin de faire grandir notre relation avec Dieu.

Comment agir pour faire route chaque jour avec Dieu, pour se convertir, pour changer nos coeurs et nos vies, pour que les priorités de Dieu s'enracinent en nous et guident notre quotidien?

La prière est ce lien intérieur qui nous permet de différentes façons de nous abreuver à la source.
La prière exige un effort, une gratuité, un désir de Dieu.

{{< hint info >}}
**Jésus dit:**  
"Quant tu pries, entre dans ta chambre, ferme ta porte, et prie ton Père qui est là dans le lieu secret; et ton Père, qui voit dans le secret, te le rendra" (Mattieu 6:6)
{{< /hint >}}

Demandons aujourd'hui à saint Charbel de nous aider à retrouver le goût de la prière afin d'aller à la rencontre de Dieu et d'être à son écoute.

## 8ème jour - Faire le bien

Le chemin de Dieu est celui du bien et de l'amour.
Il faut à travers notre vie faire des choix qui s'inspirent de la charité et de la vérité.

Voici notre défi, savoir renoncer aux ténèbres et choisir la lumière, être sans cesse en quête de ce qui est vrai et de ce qui vient de Dieu; savoir distinguer l'accessoire de l'essentiel.

Les différents choix, les différentes options, les différents virages que nous confrontons dans notre vie nous aiderons à nous rappeler que chaque jour que Dieu nous invite à faire le bien à la suite et à l'exemple de Jésus.

{{< hint info >}}
**Jésus nous invite à cette force de dépassement et du renoncement à l'égoïsme:**  
"Si ta main droite est pour toi une occasion de chute, coupe-la et jette-la loin de toi" (Mattieu 5:30)
{{< /hint >}}

Il ne s'agit pas ici de se mutiler, mais plutôt de se dépasser, à vaincre ce qui nous empêche de semer la bonté et l'amour, comme Jésus.
S'affranchir de ce qui nous empêche d'avancer sur le chemin de la charité.

Demandons aujourd'hui à saint Charbel de nous aider à discerner les demandes de Dieu qui nous invitent à bâtir son Règne en notre monde

## 9ème jour - L'austérité, chemin vers la lumière

Le mot austérité rime souvent avec sévérité, le renoncement au plaisir, à des bons moments.

Mais si l'austérité devient davantage pour nous l'expression d'un désir profond, d'une soif de Dieu, afin de toujours choisir dans notre vie l'essentiel, elle ne doit pas nous empêcher de goûter à la vie.

À travers sa mission, Jésus a pris le temps d'aller aux noces de Cana, d'aller manger avec Marthe et Marie et de se rendre disponible pour accueillir les personnes.

Il faut apprendre à s'abandonner à Dieu, à nous laisser guider par l'Esprit Saint, à faire de notre personne la maison de Dieu.

{{< hint info >}}
**Jésus, lui le Fils de Dieu a dit:**  
"Père, entre tes mains je remets mon esprit" (Luc 23:46)
{{< /hint >}}

Par cette parole, il nous apprend à faire confiance, à s'abandonner.
Un jour, à la suite de Jésus, nous habiterons chez Dieu et notre vie de chaque jour est le chemin qui nous y conduit.

Demandons aujourd'hui à saint Charbel de nous aider à trouver en nous cette austérité, ce désir constant de s'abandonner à Dieu et à lui faire confiance.
