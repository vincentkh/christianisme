# Salve Regina

Salut, reine, mère de la miséricorde.  
Vie, douceur, espérance des hommes salut !  
Enfants d’Ève, nous crions vers toi dans notre exil.  
Vers toi, nous soupirons parmi les cris et les pleurs de cette vallée de larmes  
Ô toi, notre avocate, tourne vers nous ton regard plein de bonté.  
Et montre-nous Jésus le fruit béni de tes entrailles, à l’issue de cet exil.  
Ô clémente ! Ô bonne ! Ô douce Vierge Marie.
