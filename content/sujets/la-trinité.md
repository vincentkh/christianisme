# La Trinité

## L’homme est une trinité

![Trinité de l'homme](/christianisme/trinité-de-l'homme.gif)

Corps, âme et esprit.

Si l’un ou l’autre de ces éléments manque, ce n’est plus un homme.

L’homme n’est pas capable d’imaginer un Dieu unique qui existe en trois personnes.  
C’est Dieu qui nous a révélé ce mystère de son amour par l’envoi de son Fils et du Saint-Esprit.

L'Âme est à l'image du Père.  
Le Corps est à l'image du Fils.  
L'Esprit est à l'image de l'Esprit.

## La famille est une trinité

Père, mère et enfant.
Joseph, Marie et Jésus.

## L’univers est une trinité.

Le temps, l’espace et la matière interagissent ensemble pour former tout ce qui se trouve dans l’univers, tout ce qui vit, et tout ce qui abrite la vie.

Chacun d’eux doit être créé au même moment car s’il y a de la matière mais pas d’espace alors ou la mettrez-vous ? S’il y a de la matière dans l’espace mais l’absence de temps, quand la mettrez-vous ? On ne peut avoir le temps, l’espace et la matière de manière indépendante, ils doivent être créés simultanément.

La Bible répond à cette question en 9 (3x3) mots :
Au commencement, Dieu créa les cieux et la terre.

Au commencement – il y a le temps, Dieu créa les cieux – il y a l’espace, et la terre – il y a la matière. Donc il y a le temps, l’espace et la matière qui sont une trinité des trinités.

Le temps c’est le passé, le présent et le futur.
Pour l’espace il y a longueur, largeur, hauteur.
Pour la matière il y a solide, liquide, gaz.

Le Dieu qui a créé cet univers est à l’extérieur de l’univers, il se trouve au dessus, au-delà, à travers. Il n’est pas affecté par cela.

S’il est affecté par le temps, l’espace ou la matière, alors il n’est pas Dieu.

## L’atome est une trinité

{{< hint info >}}
Le mot atome vient du grec atomos = indivisible
{{< /hint >}}

![Atome](/christianisme/atome.gif)

La trinité de l’atome qui est le constituant fondamental de la matière.

Proton, neutron et électron.

Ce que les scientifiques appellent la **trinité nucléaire.**

## L’eau est une trinité

![H2O](/christianisme/h2o.jpg)

Elle est constituée d'un atome d'oxygène relié à deux atomes d'hydrogène (H2O).

## L’air est une trinité

L'air sec se compose d'environ 78 % d'azote, 21 % d'oxygène et 1 % d'argon.

## Le feu est une trinité

![Triangle du feu](/christianisme/triangle-du-feu.png)

La réaction chimique de la combustion ne peut se produire que si l'on réunit au minimum trois éléments : un combustible, un comburant (dioxygène), et une énergie d'activation, chacun en quantité suffisante. C'est pourquoi on parle du « triangle du feu ».

## Le soleil est une trinité

Soleil, rayon et chaleur.

Le soleil, c’est le Père.  
Le rayon du soleil, c’est le Fils.  
La chaleur, c’est le Saint-Esprit.

Qui peut regarder le soleil en face ? Personne. Si vous le regardez, vous êtes ébloui.

# La couleur est une trinité

Ce sont le rouge, le jaune et le bleu.  
Elles sont dites primaires car elles ne peuvent être obtenues par le mélange d'autres couleurs.  
À l'inverse, elles servent de base aux différents mélanges, appelés couleurs secondaires.

## La plante est une trinité

Racine, tige et fruit.

## Le sable est une trinité

Les grains de sables sont le plus souvent des grains du quartz.  
Ce minéral est formé de silicium et d'oxygène.  
Sa formule est SiO2.
