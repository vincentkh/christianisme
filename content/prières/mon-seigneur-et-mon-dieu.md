# Mon Seigneur et mon Dieu

Mon Seigneur et mon Dieu, prends moi tout ce qui m’éloigne de Toi.  
Mon Seigneur et mon Dieu, donne moi tout ce qui me rapproche de Toi.  
Mon Seigneur et mon Dieu, détache moi de moi même pour me donner tout à Toi.

Catéchèse de l’église catholique
